from django.db import models

# Create your models here.
from django.db import models
from djangotoolbox.fields import EmbeddedModelField

from djangotoolbox.fields import ListField
class Post(models.Model):
    created_on = models.DateTimeField(auto_now_add=True, null=True)
    title = models.CharField(max_length=254)
    text = models.TextField(max_length=254)
    tags = ListField()
    # comments = ListField()
    comments = ListField(EmbeddedModelField('Comment'))




class Comment(models.Model):
    created_on = models.DateTimeField(auto_now_add=True)
    author = EmbeddedModelField('Author')
    text = models.TextField(max_length=254)


class Author(models.Model):
    name = models.CharField(max_length=254)
    email = models.EmailField()

# class Comment(models.Model):
#     created_on = models.DateTimeField(auto_now_add=True)
#     author_name = models.CharField(max_length=255)
#     author_email = models.EmailField()
#     text = models.TextField()