"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.test import TestCase
from nonrelblog.models import Post, Comment, Author


class SimpleTest(TestCase):
    def test_basic_addition(self):
        """
        Tests that 1 + 1 always equals 2.
        """

        self.assertEqual(1 + 1, 2)


class MongodbAccessDataTest(TestCase):
    def test_insert_post(self):
        post = Post.objects.create(
            title='Hello MongoDB!',
            text='Just wanted to drop a note from Django. Cya!',
            tags=['mongodb', 'django']
        )

        # Surely we want to add some comments.
        post.comments

        # Look and see, it has actually been saved!
        post.comments.extend(['Great post!', 'Please, do more of these!'])
        post.save()

        for record in Post.objects.all():
            print record.comments

    def test_insert_comment(self):
        from nonrelblog.models import Comment, Author
        Comment(author=Author(name='Bob', email='bob@example.org'), text='The cake is a lie').save()

        comment = Comment.objects.get()
        comment.author

    def test_insert_post_for_new_field(self):
        comment = Comment.objects.all()[0]
        Post(title='I like cake', comments=[comment]).save()
        post = Post.objects.get(title='I like cake')
        post.comments
        print post.comments[0].author.email
